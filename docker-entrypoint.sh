#!/bin/bash

# ----------------------------------------------------------------------------
# Are we the Superuser? No, thank you.
# ----------------------------------------------------------------------------
if [[ $EUID -eq 0 ]]; then
    echo "This application is not meant to be run by the superuser" 1>&2
    exit 1
fi

# Set it up
source zookeeper-setup.sh

# Kick it off
exec bin/zkServer.sh start-foreground
