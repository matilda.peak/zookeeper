# ----------------------------------------------------------------------------
# Copyright (C) 2019 Matilda Peak - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# ----------------------------------------------------------------------------

FROM registry.gitlab.com/matilda.peak/runtime-java:stable

# Labels
LABEL maintainer='Matilda Peak <info@matildapeak.com>'

ENV ZK_DATA_DIR=/var/lib/zookeeper/data \
    ZK_DATA_LOG_DIR=/var/lib/zookeeper/log \
    ZK_LOG_DIR=/var/log/zookeeper

USER root

RUN apt-get update && apt-get install netcat-traditional && apt-get clean

COPY fix-permissions /usr/local/bin

RUN curl -fsSL http://www.apache.org/dist/zookeeper/zookeeper-3.4.14/zookeeper-3.4.14.tar.gz | tar xzf - -C /opt && \
    /usr/local/bin/fix-permissions /opt/zookeeper-3.4.14 && \
    ln -s /opt/zookeeper-3.4.14 /opt/zookeeper && \
    rm -rf /opt/zookeeper/CHANGES.txt \
           /opt/zookeeper/README.txt \
           /opt/zookeeper/NOTICE.txt \
           /opt/zookeeper/CHANGES.txt \
           /opt/zookeeper/README_packaging.txt \
           /opt/zookeeper/build.xml \
           /opt/zookeeper/config \
           /opt/zookeeper/contrib \
           /opt/zookeeper/dist-maven \
           /opt/zookeeper/docs \
           /opt/zookeeper/ivy.xml \
           /opt/zookeeper/ivysettings.xml \
           /opt/zookeeper/recipes \
           /opt/zookeeper/src \
           /opt/zookeeper/zookeeper-3.4.14.jar.asc \
           /opt/zookeeper/zookeeper-3.4.14.jar.md5 \
           /opt/zookeeper/zookeeper-3.4.14.jar.sha1

COPY zookeeper-check.sh    \
     zookeeper-metrics.sh  \
     zookeeper-setup.sh    \
     docker-entrypoint.sh  \
     /opt/zookeeper/

RUN useradd --uid 6000 --system --shell /bin/bash --user-group --home-dir /opt/zookeeper zookeeper && \
    mkdir -p $ZK_DATA_DIR $ZK_DATA_LOG_DIR $ZK_LOG_DIR /usr/share/zookeeper /tmp/zookeeper && \
    chown -R zookeeper:0 /opt/zookeeper-3.4.14 $ZK_DATA_DIR $ZK_DATA_LOG_DIR $ZK_LOG_DIR /usr/share/zookeeper /tmp/zookeeper && \
    /usr/local/bin/fix-permissions $ZK_DATA_DIR && \
    /usr/local/bin/fix-permissions $ZK_DATA_LOG_DIR && \
    /usr/local/bin/fix-permissions $ZK_LOG_DIR && \
    /usr/local/bin/fix-permissions /tmp/zookeeper

WORKDIR "/opt/zookeeper"

USER zookeeper

ENTRYPOINT ["./docker-entrypoint.sh"]
