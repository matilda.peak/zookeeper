---
Title:      MatildaPeak zookeeper docker image
Author:     Alan Christie
Date:       15 July 2019
---

[![build status](https://gitlab.com/matilda.peak/zookeeper/badges/master/build.svg)](https://gitlab.com/matilda.peak/zookeeper/commits/master)

# A docker image for Apache Zookeeper
The docker file is based upon the one provided by [Kubernetes ZooKeeper contrib].

---

[Kubernetes ZooKeeper contrib]: https://github.com/kubernetes/contrib/tree/master/statefulsets/zookeeper
